package com.besaba.revonline.primitivecollections.iterables;

import com.besaba.revonline.primitivecollections.iterables.iterators.ShortIterator;

/**
 * @author Marco
 * @since 1.0
 */
public interface ShortIterable {
    ShortIterator iterator();
}

package com.besaba.revonline.primitivecollections.iterables;

import com.besaba.revonline.primitivecollections.iterables.iterators.BooleanIterator;

/**
 * @author Marco
 * @since 1.0
 */
public interface BooleanIterable {
    BooleanIterator iterator();
}

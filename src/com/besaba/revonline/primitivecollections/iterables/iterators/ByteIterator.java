package com.besaba.revonline.primitivecollections.iterables.iterators;

/**
 * @author Marco
 * @since 1.0
 */
public interface ByteIterator {
    boolean hasNext();

    byte next();
}

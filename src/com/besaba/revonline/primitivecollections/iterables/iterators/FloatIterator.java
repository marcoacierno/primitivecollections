package com.besaba.revonline.primitivecollections.iterables.iterators;

/**
 * @author Marco
 * @since 1.0
 */
public interface FloatIterator {
    boolean hasNext();

    float next();
}

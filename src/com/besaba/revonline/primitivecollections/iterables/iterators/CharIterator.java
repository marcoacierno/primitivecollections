package com.besaba.revonline.primitivecollections.iterables.iterators;

/**
 * @author Marco
 * @since 1.0
 */
public interface CharIterator {
    boolean hasNext();

    char next();
}

package com.besaba.revonline.primitivecollections.iterables;

import com.besaba.revonline.primitivecollections.iterables.iterators.ByteIterator;

/**
 * @author Marco
 * @since 1.0
 */
public interface ByteIterable {
    ByteIterator iterator();
}

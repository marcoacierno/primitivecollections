package com.besaba.revonline.primitivecollections.iterables;

import com.besaba.revonline.primitivecollections.iterables.iterators.DoubleIterator;

/**
 * @author Marco
 * @since 1.0
 */
public interface DoubleIterable {
    DoubleIterator iterator();
}

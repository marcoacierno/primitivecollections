package com.besaba.revonline.primitivecollections.function;

/**
 * @author Marco
 * @since 1.0
 */
public interface ByteConsumer {
    void accept(byte value);
}

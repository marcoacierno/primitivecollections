package com.besaba.revonline.primitivecollections.function;

/**
 * @author Marco
 * @since 1.0
 */
public interface DoubleConsumer {
    void accept(double value);
}

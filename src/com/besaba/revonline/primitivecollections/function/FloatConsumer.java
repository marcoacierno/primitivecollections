package com.besaba.revonline.primitivecollections.function;

/**
 * @author Marco
 * @since 1.0
 */
public interface FloatConsumer {
    void accept(float value);
}

package com.besaba.revonline.primitivecollections.function;

/**
 * @author Marco
 * @since 1.0
 */
public interface BooleanConsumer {
    void accept(boolean value);
}
